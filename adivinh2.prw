#include 'protheus.ch'
#include 'parmtype.ch'

/*Fonte criado, usando VsCode para fins de estudo da plataforma e linguagem por Izaias Fernandes Domingues*/
//jogo de adivinhação

User Function Adivinha()
	Local nNum := Randomize(1,100)//função que gera número aleatório, salvo engano aceita até aproximadamente 37000
	Local nChute := 0
	Local nTent := 0 // irá mostrar quantas vezes errei. 
	
	  
	While nChute != nNum
	nChute := Val(FWInputeBox("Escolha um número [1 - 100]","")) // gera uma janela digitável para se digitar um valor //// Função Val converte conteúdo para numérico. 
		If nChute == nNum
			MsgInfo("Você Acertou! - <b>" + cValToChar(nChute) + "</b><br>ERROS:" + cValToChar(nTent), "Fim de Jogo") //<b> negrito ///// cValToChar transforma string em caracter <br> pula linha, igual HTML
		ElseIf nChute > nNum 
			MsgAlert("Valor Alto","Tente Novamente")
			nTent += 1
		Else
			MsgAlert("Valor Baixo","Tente Novamente")
			nTent +=1
		EndIf	
	


Return