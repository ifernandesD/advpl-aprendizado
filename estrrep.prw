#include 'protheus.ch'
#include 'parmtype.ch'

/*Fonte criado, usando VsCode para fins de estudo da plataforma e linguagem por Izaias Fernandes Domingues*/

User Function Estrrep()
// repete uma determinada ação até que outra seja iniciada
	Local nCount 
	Local nNum := 0
	
	For nCount := 0 To 10
	
	nNum += nCount // programa fará esta ação até 10
	
	Next
	Alert ("Valor: "+ cValToChar (nNum))
/* aqui o valor impresso será 55, pois o que acontecerá será o seguinte: 
0+1+2+3+4+5+6+7+8+9+10=55*/

///////////////////////////////OU////////////////////////////////////////

	Local nCount 
	Local nNum := 0
	
	For nCount := 0 To 10 Step 2 // de dois em dois (pares)
	
	nNum += nCount // programa fará esta ação até 10
	
	Next
	Alert ("Valor: "+ cValToChar (nNum))

/* aqui o valor impresso será 30, pois o que acontecerá será o seguinte: 
0+2+4+6+8+10=30*/
Return
