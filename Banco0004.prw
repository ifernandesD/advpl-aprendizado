#Include 'protheus.ch'
#Include 'parmtype.ch'


/*User Function Banco004
Banco de Dados Sql - reclock
-autor IZAIAS FERNANDES DOMINGUES
-data 30/07/2020*/

User Function BANC004()

    Local aArea := SB1->(GetArea())

    DbSelectArea('SB1')
    Sb1->(DbSetOrder(1))//indice 1
        Sb1->(DbGoTop())

    Begin Transaction // trava tabela para uso exclusivo desta rotina, enquanto eu não sair da transação. 

            MsgInfo("A descrição do produto será alterada", "Atenção")

    If SB1->(DbSeek(FWxFilial('SB1') + '000002'))//FWXFilial para não travar uma filial unica no sistema
        RecLock('SB1', .F.) //trava registro do recLock para alteração se .T. trava pra inclusão
    Replace B1_DESC With "MONITOR DELL 42 PL" //ALTERANDO dado do sql
        SB1->(MsUnlock()) //destrava tabela reclock
    EndIf

        MdgAlert("Alteração Efetuada!", "Atenção")
    End Transaction // Finaliza transação
    RestArea(aArea)



Return