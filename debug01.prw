#include 'protheus.ch'
#include 'parmtype.ch'


/*User Function DEBUG01()
FONTE CRIADO PARA ESTUDO DO ADVPL
@autor IZAIAS FERNANDES DOMINGUES
@data 16/08/2020
@version undefined
TODO quantidade de produtos da tabela SB1
 
*/
User Function DEBUG01()
    Local aArea := GetArea()
    Local aProduto := {}
    Local nCount := 0

    //Seleciona a tabela de produtos 
    DbSelectArea("SB1")
    SB1->(DbSetOrder(1)) //Seleciona Indice
    SB1->(DbGoTop()) // seta para o início/topo


    While ! Sb1-> (EoF()) // enquanto não for final do arquivo
        aADD(aProduto, { SB1->B1_COD,;
                         SB1->B1_DESC})

        nCount++
        SB1->(DbSkip())
    EndDo

    MsgAlert("Quantidade de produtos encontrada: <b>"  cValToChar(nCount))

    RestArea(aArea)


Return