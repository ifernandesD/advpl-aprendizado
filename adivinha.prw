#include 'protheus.ch'
#include 'parmtype.ch'


// parte 1 
/*Fonte criado, usando VsCode para fins de estudo da plataforma e linguagem por Izaias Fernandes Domingues*/
//jogo de adivinhação



User Function Adivinha()
	Local nNum := 50
	Local nChute := 0 
	
	  
	While nChute != nNum
	nChute := Val(FWInputeBox("Escolha um número [1 - 100]","")) // gera uma janela digitável para se digitar um valor //// Função Val converte conteúdo para numérico. 
		If nChute == nNum
			MsgInfo("Você Acertou! - <b>" + cValToChar(nChute) + "</b>", "Fim de Jogo") //<b> negrito ///// cValToChar transforma string em caracter
		ElseIf nChute > nNum 
			MsgAlert("Valor Alto","Tente Novamente")
		Else
			MsgAlert("Valor Baixo","Tente Novamente")
		EndIf	
	


Return