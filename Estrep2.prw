#include 'protheus.ch'
#include 'parmtype.ch'

/*Fonte criado, usando VsCode para fins de estudo da plataforma e linguagem por Izaias Fernandes Domingues*/

User Function Estrep2()
// repete uma determinada ação até que outra seja iniciada
	 
	Local nNum1 := 0
	Local nNum2 := 10
	
	While nNum1 < nNum2
		nNum1++	
	
	EndDo //fechamento While
		Alert(nNum1+nNum2)
		
Return