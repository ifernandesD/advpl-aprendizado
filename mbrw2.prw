#Include 'protheus.ch'
#Include 'parmtype.ch'


/*User Function MBRSA2()
FONTE CRIADO PARA ESTUDO DO ADVPL
-autor IZAIAS FERNANDES DOMINGUES
-data 15/08/2020*/
/*Função FILBROWSE - para filtrar infos 
c - caractere
a - array - vetor
b - bloco de código {||}
@ - faz referência à

*/


User Function MBRSA2()
    Local cAlias        := "SA2"
    Local aCores        := {}
    Local cFiltra       := "A2_FILIAL == '"+xFilial('SA2')+"' .And. A@_EST == 'SP'"
    Private cCadastro   := "Cadastro  MBROWSE" // string
    Private aRotina     := {}
    Private aIndexSA2   := {}
    Private bFiltraBrw  := {|| FilBrowse(cAlias,@aIndexSA2,@cFiltra)} 



    AADD(aRotina, {"Pesquisa"   ,"AxPesqui"  ,0,1})//{"titulo","rotina-neste caso rotinas padrões do advpl para o MBROWSE" ,número reservado, operação}
    AADD(aRotina, {"Visualizar" ,"AxVisual"  ,0,2})
    AADD(aRotina, {"Incluir"    ,"U_BInclui"  ,0,3})
    AADD(aRotina, {"Trocar"     ,"U_BAltera"  ,0,4})
    AADD(aRotina, {"Excluir"    ,"U_BDeleta"  ,0,5})
    AADD(aRotina, {"Legenda"   ,"U_BLegenda",0,6})//função livre - 6=outras ações aqui para mostrar o potencial máximo suportado pelo MBROWSE

    //aCores - Legenda

    AADD(aCores,{"A2_TIPO =='F'"  ,"BR_VERDE"  }) //campo tipo é onde quero colocar a legenda F = pessoa física 
    AADD(aCores,{"A2_TIPO =='J'"  ,"BR_AMARELO"}) //J = PESSOA JURÍDICA
    AADD(aCores,{"A2_TIPO =='X'"  ,"BR_LARANJA"}) //X = exportação 
    AADD(aCores,{"A2_TIPO =='R'"  ,"BR_MARROM" }) //R = Rural
    AADD(aCores,{"Empty(A2_TIPO)" ,"BR_PRETO"  }) //vazio 

    dbSelectArea("cAlias"/*"SB1" - tanto faz */)
    dbSetOrder(1)//indice

    Eval(bFiltraBrw) //executa filtro antes do mBrowse   
    
    dbGoTop()
    mBrowse(6,1,22,75,cAlias,,,,,,aCores)//ver documentação do mBrowse, pra entender todos os espaços reservados (,,,)

    EndFilBrw(cAlias,aIndexSA2)


Return Nil
/* 
    Função BInclui - Inclusão
*/
User Function BInclui(cAlias,nReg/*registro RECNO*/,nOpc/*armazena opção 1,2,3,4,5 ou 6*/)//todo dado armazenado no protheus, precisa de um registro, que é a chave primária dele. 
    Local nOpcao := 0
    nOpcao := AxInclui(cAlias,nReg,nOpc)//desta forma conseguimos adicionar funções e ações antes do axinclui
        If nOpcao == 1 // ou 0 
            MsgInfo("Inclusão efetuada com sucesso!")
        else
            MsgAlert("Inclusão cancelada!")
        EndIf
    


Return Nil

/* 
    Função BAltera - Alteração
*/
User Function BAltera(cAlias,nReg/*registro RECNO*/,nOpc/*armazena opção 1,2,3,4,5 ou 6*/)//todo dado armazenado no protheus, precisa de um registro, que é a chave primária dele. 
    Local nOpcao := 0
    nOpcao := AxAltera(cAlias,nReg,nOpc)//desta forma conseguimos adicionar funções e ações antes do axinclui
        If nOpcao == 1 // ou 0 
            MsgInfo("Alteração efetuada com sucesso!")
        else
            MsgAlert("Alteração cancelada!")
        EndIf
    


Return Nil

/* 
    Função BDeleta - Exclusão
*/
User Function BDeleta(cAlias,nReg/*registro RECNO*/,nOpc/*armazena opção 1,2,3,4,5 ou 6*/)//todo dado armazenado no protheus, precisa de um registro, que é a chave primária dele. 
    Local nOpcao := 0
    nOpcao := AxDeleta(cAlias,nReg,nOpc)//desta forma conseguimos adicionar funções e ações antes do axinclui
        If nOpcao == 1 // ou 0 
            MsgInfo("Exclusão efetuada com sucesso!")
        else
            MsgAlert("Exclusão cancelada!")
        EndIf
    


Return Nil
/*
    Função BLegenda - Legenda
    Padrão de cores ADVPL:
    BR_AMARELO
    BR_AZUL
    BR_AZUL_CLARO
    BR_BRANCO
    BR_CINZA
    BR_LARANJA
    BR_MARROM
    BR_MARROM_OCEAN
    BR_PINK
    BR_PRETO
    BR_VERDE
    BR_VERDE_ESCURO
    BR_VERMELHO
    BR_VIOLETA
    BR_CANCEL (X EM COR VERMELHA)
*/
User Function BLegenda()
    Local aLegenda := {}


    AADD(aLegenda,{"BR_VERDE"   ,"Pessoa Física"   })
    AADD(aLegenda,{"BR_AMARELO" ,"Pessoa Jurídica" })
    AADD(aLegenda,{"BR_LARANJA" ,"Exportação"      })
    AADD(aLegenda,{"BR_MARROM"  ,"Fornecedor Rural"})
    AADD(aLegenda,{"BR_PRETO"   ,"Não Classificado"})

    BRWLegenda(cCadastro/*nome da tela*/, "Legenda"/*string ou cTitulo*/,aLegenda) 
    


Return Nil