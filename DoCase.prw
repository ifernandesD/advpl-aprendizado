#include 'protheus.ch'
#include 'parmtype.ch'

/*Fonte criado, usando VsCode para fins de estudo da plataforma e linguagem por Izaias Fernandes Domingues*/

User Function DoCase()

	//comando do case substitui uso de vários if else.

	Local cData := "25/12/2020" //data em formado string
	Do Case // faça caso
	 
	Case cData == "20/07/2020"
	Alert ("Não é Natal" + cData)
	
	Case cData == "25/12/2020"
	Alert ("É Natal")
	
	OtherWise // caso nenhum dos casos seja .T.
	MsgAlert("Não sei qual dia é hoje!")
	
	EndCase

Return