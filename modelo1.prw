#Include 'protheus.ch'
#Include 'parmtype.ch'


/*User Function  MODELO01()
atualização de dados 
Modelo 1 ou AXCADASTRO - para cadastramentos em tela cheia. ex.: Cadastro de cliente.
Modelo 2 - não recomendado ultimamente -Cadastramentos envolvendo apenas uma tabela, mas com um cabeçalho e, opcionalmente, um rodapé e um corpo com quantidade ilimitada de linhas. Ideal para casos em que há dados que se repetem por vários itens e que, por isso, são colocados no cabeçalho. Ex.: Pedido de Compra.
Modelo 3 - Cadastramentos envolvendo duas tabelas, um com os dados de cabeçalho e outro digitado em linhas com os itens. Ex.: Pedido de vendas, Orçamentos etc. 

Todos os modelos são genéricos, ou seja, o programa independe da tabela a ser tratada, bastando praticamente que se informe apenas o seu Alias. O resto é obtido do Dicionário de dados (SX3). 
-autor IZAIAS FERNANDES DOMINGUES
-data 30/07/2020*/



User Function MODELO1()
    Local cAlias := "SB1"
    Local cTitulo := "Cadastro - AXCadastro"
    Local cVldExc := ".T."
    Local cVldAlt := ".T."

    AXCadastro(cAlias, cTitulo,cVldExc,cVldAlt) // (o cAlias, com um cTitulo, eu autorizo a cVldExc exclusão, e autorizo a VldAlt alteração)
    

Return Nil