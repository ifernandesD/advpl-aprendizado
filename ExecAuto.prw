#Include 'protheus.ch'
#Include 'parmtype.ch'


/*User Function Banco005
Banco de Dados Sql - MsExecAuto
-autor IZAIAS FERNANDES DOMINGUES
-data 30/07/2020*/



User Function BANC005()

    Local aArea := GetArea()
    Local aDados := {}
    Private lMSErroAuto := .F. //var locica que recebe valor false, onde ficará armazenada o erro, iniciar em false sempre 

    aDados: {;
                {"B1_COD",   "111111",        Nil}, ;//NIL= VALOR não especificado
                {"B1_DESC", "PRODUTO TESTE",  Nil},;
                {"B1_TIPO", "GG",             Nil},;
                {"B1_UM", "PC",               Nil},;
                {"B1_LOCPAD", "01",           Nil},;
                {"B1_PICM", 0,                Nil},;
                {"B1_IPI", 0,                 Nil},;
                {"B1_CONTRAT", "N",           Nil},;
                {"B1_LOCALIZ", "N",           Nil};
            }
    //Início do controle de transação
    Begin Transaction 
    // chama cadastro de produtos de forma automática MATA010
        MsExecAuto({|x,y|Mata010(x,y)},aDados,3) //semelhante bloco de código,, ({|insita os parâmetros,neste caso X e Y, podendo colocar-se mais| rotina automática que irá executar, passando por parâmetros as variaveis declaradas nos pipes, neste caso MATA010(x,y)}, informar quais dados vc quer passar pro MATA010, ou seja, qual é a variavel x e qual é a y, neste caso o x é aDados, e também preciso passar o numero da operação, neste caso 3 = Incluir (4=Alteração e 5=Exclusão)

        //caso ocorra algum erro 
            If lMSErroAuto//.T.
                Alert("Ocorreram erros durante a operação!")
                MostraErro()
                //irá parar todo o processo e mostrará o erro. 
                DisarmTransaction()
            Else 
                MsgInfo("Operação finalizada!", "Aviso")
            EndIf
            End Transaction

            RestArea(aArea)
            


Return
