#include 'protheus.ch'
#include 'parmtype.ch'

/*Fonte criado, usando VsCode para fins de estudo da plataforma e linguagem por Izaias Fernandes Domingues*/

User Function ESTRUTUR()
//estrutura de decisão
	Local nNum1 := 22 
	Local nNum2 := 100

	If (nNum1 <= nNum2)
	MsgInfo("A variável nNum1 é menor ou igual a nNum2")
	
	Else
	Alert("A variável nNum1 não é menor ou igual a nNum2")
	EndIf // finaliza comando If
// ElseIf

	If (nNum1 = nNum2)
	MsgInfo("A variável nNum1 igual a nNum2")
	
	ElseIf (nNum1 > nNum2) // se não se 
	MsgAlert ("A variável é maior")
	ElseIf (nNum1 != nNum2)
	MsgInfo("A variável nNum1 é diferente de nNum2")
	EndIf

return