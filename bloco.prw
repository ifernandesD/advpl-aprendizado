#include 'protheus.ch'
#include 'parmtype.ch'

/*Fonte criado, usando VsCode para fins de estudo da plataforma e linguagem por Izaias Fernandes Domingues*/

User Function BLOCO()

Local bBloco := {|| Alert("Olá Mundo!")} // pipes são obrigatórios "||" mesmo que sejam vazios. alt+5555 ou alt+124 
	Eval (bBloco) // Eval chama/executa bloco
//passagem por parâmetros - Blocos de códigos
/* OU */	

Local bBloco1 := {│cMsg│ Alert (cMsg)}
		Eval (bBloco,"Olá Mundo!") 

// este fonte gerará dois pop-up's iguais. 

Return