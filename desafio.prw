#Include "Totvs.ch"

/*User Function DESAFIO
Tela de Cadastro de Cliente
-autor IZAIAS FERNANDES DOMINGUES
-data 30/07/2020*/

User Function DESAFIO()
	Local aArea     := GetArea()
	Local cAliasAx  := "SZ1"
	Local cTituloAx := "Cadastro de Cliente"
	Local cVldExc   := "u_zVldDel()"
	Local cVldOk    := "u_zVldOK()"
	
	//Chama a tela de cadastro
	AxCadastro(cAliasAx, cTituloAx, cVldExc, cVldOk)
	
	RestArea(aArea)
Return


//Validacao de Exclusao na funcao DESAFIO


User Function zVldDel()
	Local aArea     := GetArea()
	Local lContinua := .F.
	
	//Mostra uma mensagem de confirmação
	lContinua := MsgYesNo("Deseja continuar com a exclusao?", "Atencao")
	
	RestArea(aArea)
Return lContinua


//Validacao na Inclusao / Alteracao na funcao DESAFIO


User Function zVldOK()
	Local aArea     := GetArea()
	Local lContinua := .F.
	
	//Se for Inclusão, pergunta se confirma

	lContinua := MsgYesNo("Deseja continuar com a inclusao?", "Atencao")
//Senão, pergunta se confirma
	
	RestArea(aArea)
Return lContinua
