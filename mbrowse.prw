#Include 'protheus.ch'
#Include 'parmtype.ch'


/*User Function MBRW00()
FONTE CRIADO PARA ESTUDO DO ADVPL
-autor IZAIAS FERNANDES DOMINGUES
-data 15/08/2020*/

/* MBROWSE()
A mBROWSE() é uma funcionalidade que permite a utilização de recursos mais aprimorados na visualização
e manipulação das informações do sistema, possuindo os seguintes componentes.

*Browse padrão para visualização das informações da base de dados de acordo com as configurações do SX3 -
Dicionário de dados (campo browse).

*Parametização para funções específicas para as ações de visualização, inclusão, alteração e exclusão de
informações o que viabiliza a manutenção de informações com estrutura de cabeçalhos e itens. 

*Recursos adicionais como identificadores de status de registros, legendas e filtros para as informações. 
*/

User Function MBRW00()
    Local cAlias:= "SB1"
    Private cTitulo := "Cadastro Produtos MBROWSE"
    Private aRotina := {}

    AADD(aRotina, {"Pesquisa"   ,"AxPesqui"  ,0,1})//{"titulo","rotina-neste caso rotinas padrões do advpl para o MBROWSE" ,número reservado, operação}
    AADD(aRotina, {"Visualizar" ,"AxVisual"  ,0,2})
    AADD(aRotina, {"Incluir"    ,"AxInclui"  ,0,3})
    AADD(aRotina, {"Trocar"     ,"AxAltera"  ,0,4})
    AADD(aRotina, {"Excluir"    ,"AxDeleta"  ,0,5})
    AADD(aRotina, {"OlaMundo"   ,"U_OLAMUNDO",0,6})//6=outras ações aqui para mostrar o potencial máximo suportado pelo MBROWSE

    dbSelectArea("cAlias"/*"SB1" - tanto faz */)
    dbSetOrder(1)//indice
    mBrowse(,,,,cAlias)
    //mBrowse(6,1,22,75,cAlias) - outra forma do script acima 


Return Nil