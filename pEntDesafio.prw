#Include 'protheus.ch'
#Include 'parmtype.ch'


/*User Function A010TOK() - PONTOS DE ENTRADA
FONTE CRIADO PARA ESTUDO DO ADVPL
-autor IZAIAS FERNANDES DOMINGUES
-data 15/08/2020
DESAFIO 
O cliente solicita o desenvolvimento de uma tratativa no sistema ERP da empresa, 
onde não pode ser permitido que o usuário inclua produtos do tipo "PA" com a conta Contábil "001"
A010TOK função disponível em pontos de entrada da TOTVS
*/

User Function A010TOK() // A010TOK é uma função padrão TOTVS, que estamos alterando para nsso uso
    Local lExecuta := .T. // variavel padrão tdn 
    Local cTipo := AllTrim(M->B1_TIPO)//limpa todos os espaços em branco de uma string // M-> memória 
    Local cConta := AllTrim(M->B1_CONTA)

        If (cTipo = "PA" .AND. cConta = "001") //<b> </b> comandos html para personalização (negrito, itálico, etc...)
            Alert("A conta <b> "+ "cConta + </b> não pode estar"+ ;
            "associada a um produto do tipo<b>" + cTipo)
            lExecuta := .F.
        EndIf


Return (lExecuta) // é importante para que funcione, pois se não não irá retornar da forma que queremos, bloqueando o programa, e o user conseguirá alterar, fazendo com que o programa seja inútil. 
