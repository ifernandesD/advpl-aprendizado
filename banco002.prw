#Include 'protheus.ch'
#Include 'parmtype.ch'

/*User Function Banco002
Banco de Dados Sql
-autor IZAIAS FERNANDES DOMINGUES
-data 30/07/2020*/

User Function BANCO002()
    Local aArea := SB1->(GetArea())
    Local cMsg := ''

    DbSelectArea("SB1")
    SB1->(DbSetOrder(1)) // Posiciona no indice // DbSetOrderNickName() para posicionar pelo Nickname
    SB1->(DbGoTop())//-> seta setar

    cMsg := Posicione(  'SB1',;
                             1,;
                             FWXfilial('SB1')+ '000002',;
                             'B1_DESC'  ) //Posicione retorna um unico campo escolhido //SB1 é o alias
    //; faz com que linha seguinte de codigo continue como se fosse uma só.
    //FWXFilial() filial corrente 
    //B1_DESC campo de retorno (que queremos trazer)
    Alert("Descrição Produto: " +cMsg, "AVISO")

    RestArea(aArea) //recomendável para fechar a area. 


Return